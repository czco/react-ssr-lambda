const express = require('express')
const next = require('next')
const port = 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const logMessage = (message) => {
  console.log(`${new Date()} - ${message}`)
}

const createServer = () => {
  const server = express()
  server.get('/p/:id', (req, res) => {
    console.log('request intercepted')
   const actualPage = '/post'
   const queryParams = { title: req.params.id }
   app.render(req, res, actualPage, queryParams)
 })
  server.get('*', (req, res) => handle(req, res))
  return server
}

if (process.env.IN_LAMBDA) {
  module.exports = createServer()
} else {
  app.prepare().then(() => {
    const server = createServer()
    server.listen(port)
  })
}
