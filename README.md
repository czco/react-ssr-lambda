# Isomorphic React app running in Lambda for SSR

Using `express`, `aws-serverless-express`, `claudia` and `next`

SSR | Isomorphic | Universal React using Lambda and API gateway

### First Deploy

```bash
npm i -g yarn
npm i -g claudia
yarn
yarn build
claudia create --region eu-west-1 --handler lambda.handler --deploy-proxy-api
```
### Updates after first deploy
```bash
claudia update
```


![alt text](https://preview.ibb.co/nvJxUT/2018_07_26_17_46_09.png")



#### Next 4.2.3

Not tried with any other versions

#### Credit
All credit goes to Jonathan Gautheron
